var ng_module_register = ['ngRoute'];
var app = angular.module('0x', ng_module_register);
app.directive('oxfooter', function(){
    var url = '<a href="'+ configuration['app']['url'] +'">' + configuration['app']['name'] + '</a>';
    var oxurl = '<a href="/https://github.com/0xpy/">0xpy</a>';
    var footer = ['&copy;', moment().year(), url, '| Powered by', oxurl];

    return {
        "restrict": "E",
        "template": footer.join(' ')
    }
});
