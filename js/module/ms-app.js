// ____main module angularjs____
var app = angular.module('MSApp', ['ngRoute', 'ngFileUpload', 'ngCookies']);

app.filter('range', function() {
    return function(iterate, max) {
        max = parseInt(max);
        for (var i = 0; i < max; i++) {
            iterate.push(i);
        }

        return iterate;
    };
});

// ____http factory angularjs____
app.factory('Http', function($http, Upload){
    var requestHTTP = function(target, data, request_type='text'){
        var param_ = {
            "url": target,
            "method": data.method,
            "headers": {
                "Content-Type": 'application/json; charset=utf-8',
                "Accept": 'application/json'
            }
        }

        // ____if use authentication token____
        if(typeof data.authentication !== 'undefined'){
            param_.headers['Authorization'] = 'Bearer ' + data.authentication;
        }

        // ____if using the data parameter____
        if(typeof data.data !== 'undefined'){
            param_.data = data.data;
        }

        if(request_type == 'upload'){
            return Upload.upload(param_)
        } else {
            return $http(param_)
        }
    }

    return {
        "upload": function(method, target, data){
            var data = data;
            data['method'] = method;

            // -- sending xhr with params --
            return requestHTTP(target, data, 'upload');
        },
        "send": function(method, target, data){
            var data = data;
            data['method'] = method;

            // -- sending xhr with params --
            return requestHTTP(target, data, 'text');
        },
        "sendGet": function(target, authentication=null){
            var data = {method: 'get'};

            if (authentication != null){
                data['authentication'] = authentication;
            }

            // -- sending xhr with params --
            return requestHTTP(target, data, 'text');
        }
    }
})
