app.controller("MSSidebar", ["$scope", function($scope){

    $scope.menus = [
        {
            "label": "Dashboard",
            "href":"#!/",
            "tooltip": "Dashboard application MSC",
            "target": "",
            "menus": []
        }, {
            "label": "Users",
            "href": "",
            "tooltip": "User management MSC",
            "target": "",
            "menus": [
                {
                    "label": "All users",
                    "href":"#!/user",
                    "tooltip": "Show all user from MSC",
                    "target": "",
                    "menus": []
                },{
                    "label": "Create a user",
                    "href":"#!/user/create",
                    "tooltip": "Create a users",
                    "target": "",
                    "menus": []
                }
            ]
        }
    ];
    $scope.menus.push({
        "label": "Menus",
        "href": "",
        "tooltip": "Menus management MSC",
        "target": "",
        "menus":[{
            "label": "All menus",
            "href": "",
            "tooltip": "Show all menus from MSC",
            "target": "",
            "menus":[]
        }],
    });
}]);