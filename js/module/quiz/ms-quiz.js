app.controller('MsQuiz', ['$scope', 'Http', '$routeParams', function($scope, Http, $routeParams){
    var available_pages = {
        'list': '/pages/quiz/list.html',
        'create': '/pages/quiz/create.html'
    }

    var loadList = function(){
        Http.sendGet(apiUrl + 'quiz')
            .then(function success(response){
                // ____when the response is successful____
                var data = response.data;

                // ____loading result data into scope data angularjs____
                $scope.data = data.data;
            },
            function error(response){
                var data = response.data;
            });
    }

    // ____options choice____
    $scope.type_select = [
        {"value": "essay", "label": "Essay"},
        {"value": "multiple_choice", "label": "Pilihan Ganda"},
    ]
    $scope.type_multiple_choice = ['A','B','C','D','E'];

    if($routeParams['id'] != null){
        $scope.page = available_pages['detail'];
    } else if($routeParams['module'] == 'create') {
    } else {
        $scope.page = available_pages['list'];
        $scope.init = loadList();
    }
}]);