app.controller('MsProject', ['$scope', '$cookies','$window', 'Http', '$routeParams', function($scope, $cookies, $window, Http, $routeParams){
    var available_pages = {
        'show': '/pages/project/table.html',
        'create': '/pages/project/create.html',
        'detail': '/pages/project/detail.html'
    }
    
    if($routeParams['page'] != null){
        $scope.page = available_pages[$routeParams['page']];
    } else {
        $scope.page = available_pages['show'];
    }
    $scope.id = $routeParams['id'];
    $scope.form = {
        "name": null,
        "keyword": null,
        "file": null
    };

    $scope.data = [
        {'id': 1, 'title': 'bookworm', 'keyword': 'covid19', 'is_active': 1},
        {'id': 2, 'title': 'bookworm', 'keyword': 'testing, testing1, testing2, testing3', 'is_active': 1},
    ];

    $scope.redirect = function(url){
        return $window.location.href = url;
    }

    $scope.init = function(){
        Http.sendGet(apiUrl+'project/').then(function success(response) {
            var data = response.data;
            $scope.data = data;
        }, function error(response){
            var data = response.data;
            console.log(data);
        });
    }

    $scope.detail = function(i){
        $scope.item = $scope.data[i];
    }

    $scope.post = function(){
        var data = {
            'name': $scope.form.name,
            'keyword': $scope.form.keyword
        }

        Http.send('post', apiUrl + 'project/', {"data": data}).then(function success(response){
            var data = response.data;
            if (data.status == 'success'){
                $window.location.href = '/dashboard.html#!/projects';
            }
        },function error(response){
            var data = response.data;
            $scope.data = data;
        });
    }
    
    $scope.upload = function(){
        var data = {
            'file': $scope.form.file
        }

        Http.upload('post', apiUrl + 'project/', {"data": $scope.form}).then(function success(response){
            var data = response.data;

            if (data.status == 'success'){
                $window.location.href = '/dashboard.html#!/projects';
            }
        },function error(response){
            var data = response.data;
            $scope.data = data;
        });
    }

}]);