app.controller('MsUser', ['$scope', '$routeParams', function($scope, $routeParams){
    var available_pages = {
        'detail': '/pages/user/detail.html',
        'list': '/pages/user/list.html'
    }

    if($routeParams['id'] != null){
        $scope.page = available_pages['detail'];
    } else {
        $scope.page = available_pages['list'];
    }
    
}]);