app.controller('MsProfile', ['$scope', '$cookies','$window', 'Http', '$routeParams', function($scope, $cookies, Http, $window, $routeParams){
    var available_pages = {
        'form': '/pages/profile/form.html',
        'upload': '/pages/profile/upload.html'
    }
    
    $scope.user_registration_profile = {};
    $scope.page = available_pages[$routeParams['step']];
    var token = $cookies.get('token');
    console.log(token);
    // ____sending the post request to login____
    $scope.post = function(){
        var message = {
            'success': 'Profile anda berhasil disimpan!',
            'failed': 'Terjadi suatu kesalahan!',
        };

        Http.send('post', apiUrl + 'user-registration-profile', {"data": $scope.user_registration_profile, "authentication": token})
            .then(function success(response){
                // ____this function is represent what happen if respond header success
                var data = response.data;
                console.log(data);
                $scope.alert = {
                    'status': 'success',
                    'message': message[data.message]
                }
                id=window.setTimeout ("alert('Tunggu sebentar')", 3000);
                $window.location.href = '#!/profile/upload.html';
            }, function error(response){
                // ____this function is represent what happen if respond header error/failure
                // ____if response is failed____
                var data = response.data;
                $scope.alert = {
                    'status': 'danger',
                    'message': message[data.message]
                }
            }
        );
    }
    
}]);