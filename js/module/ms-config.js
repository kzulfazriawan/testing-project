var apiUrl = 'http://localhost:5000/api/';

app.config(function($routeProvider) {
    $routeProvider.when("/login", {
        "templateUrl": "/pages/login.html",
        "controller" : "MsLogin"
    })
    // ____shows all projects
    .when("/projects", {
        "templateUrl": "/pages/project.html",
        "controller" : "MsProject"
    })
    // ____/project/create
    .when("/project/:page", {
        "templateUrl": "/pages/project.html",
        "controller" : "MsProject"
    })
    // ____/project/edit|delete|detail/number_id
    .when("/project/:page/:id", {
        "templateUrl": "/pages/project.html",
        "controller" : "MsProject"
    });
})