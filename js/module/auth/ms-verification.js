app.controller('MsVerification', ['$scope', 'Http', '$window', '$location', '$routeParams',
    function($scope, Http, $window, $location, $routeParams){
        $scope.progress = null;
        $scope.alert = null


        var verify_token = function(access_token){
            Http.send('post', apiUrl + 'auth/verification', {"authentication": access_token})
                .then(function success(response){
                    $scope.progress.message = 'Verifikasi akun sukses'
                    console.log(response);
                });
        }

        // ____ initial function for verification____
        $scope.init = function(){
            var message = {
                'success': 'Selamat anda telah terverifikasi',
                'failed': 'Terjadi suatu kesalahan!',
                'token_mismatch': 'Token tidak sesuai/kadaluarsa!'
            };

            Http.send('post', apiUrl + 'authentication/verification', {'data': {'token': $routeParams['code']}})
                .then(function success(response){
                    var data = response.data;
                    $scope.alert = {
                        'status': 'success',
                        'message': message[data.message]
                    }
                }, function failed(response){
                    // ____if response is failed____
                    var data = response.data;
                    $scope.alert = {
                        'status': 'danger',
                        'message': message[data.message]
                    }
                });
        }
    }
]);
