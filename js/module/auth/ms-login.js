app.controller('MsLogin', ['$scope', '$cookies', 'Http', '$window',
    function($scope, $cookies, Http, $window){
        $scope.alert = null;

        $scope.login = {
            "email": "",
            "password": "",
            "remember": false
        }

        // ____sending the post request to login____
        $scope.post = function(){
            var message = {
                'success': 'Selamat datang! Anda akan diarahkan dalam 3 detik',
                'failed': 'Terjadi suatu kesalahan!',
                'user_not_found': 'Akun anda tidak terdaftar/Otorisasi salah',
                'token_expired': 'Token kadaluarsa',
                'token_invalid': 'Token ditolak',
                'unauthorized': 'Otorisasi ditolak'
            };

            Http.send('POST', apiUrl + 'authentication/login', {"data": $scope.login})
                .then(function success(response){
                    // ____this function is represent what happen if respond header success
                    var data = response.data;
                    $scope.alert = {
                        'status': 'success',
                        'message': message[data.message]
                    }
                    
                    $cookies.put('token', data.data.token);

                    // ____if data success then it create cookies token and redirect to index
                    setTimeout(function(){
                        $window.location.href = '/dashboard.html';
                    }, 3000);
                }, function error(response){
                    // ____this function is represent what happen if respond header error/failure
                    // ____if response is failed____
                    var data = response.data;
                    $scope.alert = {
                        'status': 'danger',
                        'message': message[data.message]
                    }
                }
            );
        }
    }
]);