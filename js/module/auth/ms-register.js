app.controller('MsRegister', ['$scope', 'Http', '$window', '$location', '$routeParams',
    function($scope, Http, $window, $location, $routeParams){
        $scope.alert = null;
        
        var available_page = {
            "form": "/pages/register/form.html",
            "complete": "/pages/register/complete.html"
        };

        $scope.register = {"name": "", "email": "", "password": "", "password_confirmation": ""};
        $scope.page_include = available_page[$routeParams['page']]

        // ____sending the post request to register
        $scope.post = function(){

            var message = {
                'success': 'Selamat! Akun anda telah terdaftar! Silahkan pilih tombol masuk',
                'failed':'Terjadi kesalahan'

            };

            Http.send('POST', apiUrl + 'authentication/register', {"data": $scope.register})
                .then(function success(response){

                    var data = response.data;

                    $scope.alert = {
                        'status': 'success',
                        'message': message[data.message]
                    };

                    

                }, function error(response){
                    var data = response.data;
                    $scope.alert = {
                        'message': data.message, 
                        'status': 'danger'
                    };

                }
            );
        }
    }
]);
