var configuration = {
    "app": {
        "name": "Peer-Tweet",
        "version": "0.1",
        "url": "#"
    },
    "router": {
        "/": {"templateUrl": "/pages/dashboard/content.html", "controller": "Dashboard"},
        "/projects": {"templateUrl": "/pages/projects/content.html", "controller": "Projects"}
    }
}

app.config(function($routeProvider) {

    // ____iteration config router____
    for (var i in configuration.router) {
        $routeProvider.when(i, configuration.router[i]);
    }
});